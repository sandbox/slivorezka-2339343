/**
 * @file
 * Ajax for form.
 */
(function($){
  Drupal.behaviors.quick_edit_node_title = {
    attach: function (context, settings) {
      $('.quick-edit-node-title .pager li a', context).click(function () {
        QuickEditNodeTitleTableAjaxReload($(this, context).attr('href'));
        return false;
      });
      $('.quick-edit-node-title thead a', context).click(function () {
        QuickEditNodeTitleTableAjaxReload($(this, context).attr('href'));
        return false;
      });
    }
  };
})(jQuery);

/**
 * Ajax reload table.
 */
function QuickEditNodeTitleTableAjaxReload(href) {
  var ajax = new Drupal.ajax(
    false,
    '#quick-edit-node-title-ajax',
    {
      url : href.replace('admin/content/quick-edit-node-title', 'admin/content/quick-edit-node-title/ajax'),
      effect: 'fade'

    }
  );
  ajax.eventResponse(ajax, {});
}
